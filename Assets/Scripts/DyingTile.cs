﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DyingTile : MonoBehaviour
{
    private Renderer renderer;
    float timerBeforeDestroy = 2f;
    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<Renderer>();
        if (gameObject.GetComponent<Rigidbody2D>() == null)
            gameObject.AddComponent<Rigidbody2D>();
        gameObject.GetComponent<Collider2D>().enabled = false;
        gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-200, 200), Random.Range(50, 200)));
    }

    // Update is called once per frame
    void Update()
    {
        timerBeforeDestroy = (timerBeforeDestroy > 0) ? timerBeforeDestroy - Time.deltaTime : 0 ;
        if (timerBeforeDestroy <= 0)
            Destroy(gameObject);
        
    }
}
