﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public static Timer instance;
    private float time;
    private float onFreeze = 0;

    private TMPro.TextMeshProUGUI timerText;


    public void AddFreezeTime(float powerupTime)
    {
        onFreeze += powerupTime;
    }

    void Awake()
    {
        instance = GetComponent<Timer>();
        timerText = GetComponentInChildren<TMPro.TextMeshProUGUI>();
    }

    // Start is called before the first frame update
    void Start()
    {
        time = Settings.Instance.SecondsToSolve;
    }

    // Update is called once per frame
    void Update()
    {
        if (BoardManager.Instance.GameState == GameState.GameOver)
            SetGameOverScreen();
        if (BoardManager.Instance.GameState == GameState.Normal)
        {
            if (onFreeze <= 0)
                time -= Time.deltaTime;
            else
                onFreeze -= Time.deltaTime;
            UpdateTimer();
        }
        
    }

    private void SetGameOverScreen()
    {
        timerText.text = "Game Over";
    }

    private void UpdateTimer()
    {
        if (time <= 0)
        {
            time = 0;
            BoardManager.Instance.SetGameOver();
        }

        if (time <= 60)
        {
            timerText.text = (time % 60).ToString("00");
            timerText.color = Color.red;
        }
        else
            timerText.text = /*"Timer \n" +*/ Mathf.FloorToInt(time / 60f).ToString("00")
                                                                                    + ":"
                                                                                    + (time % 60).ToString("00");

    }
}
