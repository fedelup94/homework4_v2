﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SendToHighscoreSceneButton : MonoBehaviour
{
    void OnMouseDown()
    {
        SceneManager.LoadScene(2);
    }
}
