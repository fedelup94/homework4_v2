﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PowerClickButton : MonoBehaviour
{

    [SerializeField]
    private UnityEvent powerPick;

    private void OnMouseDown()
    {
        powerPick.Invoke();
    }
}
