﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AudioClip { Swap, Clear, Select, Bomb, Freeze};

public class AudioManager : MonoBehaviour
{

    private static AudioManager instance;
    private AudioSource[] sfx;

    public static AudioManager Instance { get => instance; }


    private void Awake()
    {
        if (instance != null && instance != this)
            Destroy(this.gameObject);
        else
            instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        sfx = GetComponents<AudioSource>();
        instance = GetComponent<AudioManager>();
    }


    public void PlaySound(AudioClip audioClip)
    {
        sfx[(int)audioClip].Play();
    }
}
