﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum GameState { Normal, OnPause, GameOver };

public class BoardManager : MonoBehaviour
{

    private static BoardManager instance;
    private Settings settings;

    [SerializeField]
    private Transform timer;
    [SerializeField]
    private Transform scoreObject;

    private GameObject[,] grid;
    private Tile[,] tileGrid;

    private List<Tile> highlightedTiles = new List<Tile>();

    private TMPro.TextMeshProUGUI scoreText;


    private GameState gameState;
    private bool isComputing = false;

    private float hintTimer = 0;


    //public Sprite GridSprite(int x, int y) { try { return grid[x, y].GetComponent<SpriteRenderer>().sprite; } catch (ArgumentOutOfRangeException exception) { throw new ArgumentOutOfRangeException("Can't access tile on x: " + x + " y: " + y, exception); } }
    public Sprite GridSprite(int x, int y) { try { return tileGrid[x, y].Renderer.sprite; } catch (ArgumentOutOfRangeException exception) { throw new ArgumentOutOfRangeException("Can't access tile on x: " + x + " y: " + y, exception); } }

    public bool IsComputing { get => isComputing; set => isComputing = value; }
    public int GridW { get => settings.GridW; }
    public int GridH { get => settings.GridH; }
    public GameState GameState { get => gameState; private set => gameState = value; }
    public static BoardManager Instance { get => instance; }


    private void Awake()
    {
        if (instance != null && instance != this)
            Destroy(this.gameObject);
        else
            instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        GameState = GameState.Normal;
        settings = Settings.Instance;
        hintTimer = settings.SecondsBeforeHint;
        Vector2 offset = settings.TilePrefab.GetComponent<SpriteRenderer>().bounds.size;
        Debug.Log(offset.x + " : " + offset.y);
        CreateBoard(offset.x, offset.y);
        scoreText = scoreObject.GetComponentInChildren<TMPro.TextMeshProUGUI>();
    }

    private void CreateBoard(float xOffset, float yOffset)
    {

        grid = new GameObject[GridW, GridH];
        tileGrid = new Tile[GridW, GridH];

        float startX = transform.position.x;
        float startY = transform.position.y;

        Sprite[] prevLeft = new Sprite[GridH];
        Sprite prevBottom = null;

        for (int x = 0; x < GridW; x++)
        {
            for (int y = 0; y < GridH; y++)
            {
                GameObject newTile = Instantiate(settings.TilePrefab, new Vector3(startX + (xOffset * x),
                                                                   startY + (yOffset * y), 0),
                                                                   settings.TilePrefab.transform.rotation);
                grid[x, y] = newTile;
                newTile.transform.parent = transform;
                List<Sprite> spawnableSprites = new List<Sprite>();
                spawnableSprites.AddRange(settings.SpawnableObjects);
                spawnableSprites.Remove(prevLeft[y]);
                spawnableSprites.Remove(prevBottom);
                Sprite newSprite;
                if (UnityEngine.Random.Range(0, 100) <= settings.PowerupSpawnChance)
                    newSprite = settings.PowerUp;
                else
                    newSprite = spawnableSprites[UnityEngine.Random.Range(0, spawnableSprites.Count)];

                newTile.GetComponent<SpriteRenderer>().sprite = newSprite;
                newTile.name = x + " " + y;
                tileGrid[x, y] = newTile.GetComponent<Tile>();
                prevLeft[y] = newSprite;
                prevBottom = newSprite;
            }
        }
    }

    public void SetOnPause()
    {
        GameState = GameState.OnPause;
        gameObject.SetActive(false);
    }

    public void UnPause()
    {
        GameState = GameState.Normal;
        gameObject.SetActive(true);

    }

    public IEnumerator FindNulltiles()
    {
        bool keepFalling = true;
        while (keepFalling)
        {
            for (int x = 0; x < GridW; x++)
            {
                for (int y = 0; y < GridH; y++)
                {
                    if (tileGrid[x,y].Renderer.sprite == null)
                    {
                        yield return StartCoroutine(ShiftTilesDown(x, y));
                        break;
                    }
                }
            }
            keepFalling = false;

            for (int x = 0; x < GridW; x++)
            {
                for (int y = 0; y < GridH; y++)
                {
                    if (tileGrid[x, y].Renderer.sprite == null)
                        keepFalling = true;
                    tileGrid[x,y].ComputeMatches();
                }
            }
        }
        
    }

    public List<Tile> GetFirstMatchAvailable()
    {
        List<Tile> possibleMatches = new List<Tile>();
        for (int x = 0; x < GridW; x++)
        {
            for (int y = 0; y < GridH; y++)
            {
                //check for vertical
                // O
                // X
                // X
                if (y < GridH - 2)
                {
                    if (GridSprite(x,y) == GridSprite(x, y + 1))
                    {
                        //check top
                        if (y < GridH - 3)
                        {
                            if (GridSprite(x,y) == GridSprite(x, y + 3))
                            {
                                possibleMatches.Add(tileGrid[x, y]);
                                possibleMatches.Add(tileGrid[x, y + 1]);
                                possibleMatches.Add(tileGrid[x, y + 3]);
                                return possibleMatches;
                            }
                        }
                        //check right
                        if (x < GridW - 1)
                            if (GridSprite(x,y) == GridSprite(x + 1, y + 2))
                            {
                                possibleMatches.Add(tileGrid[x, y]);
                                possibleMatches.Add(tileGrid[x, y + 1]);
                                possibleMatches.Add(tileGrid[x + 1, y + 2]);
                                return possibleMatches;
                            }
                        //check left
                        if (x > 0)
                            if (GridSprite(x,y) == GridSprite(x - 1, y + 2))
                            {
                                possibleMatches.Add(tileGrid[x, y]);
                                possibleMatches.Add(tileGrid[x, y + 1]);
                                possibleMatches.Add(tileGrid[x - 1, y + 2]);
                                return possibleMatches;
                            }
                    }
                }
                //check for vertical
                // X
                // X
                // O
                if (y < GridH - 2)
                {
                    if (GridSprite(x,y + 1) == GridSprite(x, y + 2))
                    {
                        //check left
                        if (x > 0 && y > 0 && GridSprite(x - 1, y) == GridSprite(x, y + 1))
                        {
                            possibleMatches.Add(tileGrid[x - 1, y]);
                            possibleMatches.Add(tileGrid[x, y + 1]);
                            possibleMatches.Add(tileGrid[x, y + 2]);
                            return possibleMatches;
                        }
                        // check bottom
                        if (y > 0)
                        {
                            if (GridSprite(x, y + 1) == GridSprite(x, y - 1))
                            {
                                possibleMatches.Add(tileGrid[x, y - 1]);
                                possibleMatches.Add(tileGrid[x, y + 1]);
                                possibleMatches.Add(tileGrid[x, y + 2]);
                                return possibleMatches;
                            }
                        }
                        //check right
                        if (x < GridW - 2)
                        {
                            if (GridSprite(x, y + 1) == GridSprite(x + 1, y))
                            {
                                possibleMatches.Add(tileGrid[x + 1, y]);
                                possibleMatches.Add(tileGrid[x, y + 1]);
                                possibleMatches.Add(tileGrid[x, y + 2]);
                                return possibleMatches;
                            }
                        }
                    }
                }
                //check for vertical 
                //X
                //O
                //X
                if (y < GridH - 2)
                {
                    if (GridSprite(x,y) == GridSprite(x, y + 2))
                    {
                        //check middle left
                        if (x > 0)
                        {
                            if (GridSprite(x,y) == GridSprite(x - 1, y + 1))
                            {
                                possibleMatches.Add(tileGrid[x, y]);
                                possibleMatches.Add(tileGrid[x - 1, y + 1]);
                                possibleMatches.Add(tileGrid[x, y + 2]);
                                return possibleMatches;
                            }
                        }
                        //check middle right
                        if (x < GridW - 2)
                        {
                            if (GridSprite(x,y) == GridSprite(x + 1, y + 1))
                            {
                                possibleMatches.Add(tileGrid[x, y]);
                                possibleMatches.Add(tileGrid[x + 1, y + 1]);
                                possibleMatches.Add(tileGrid[x, y + 2]);
                                return possibleMatches;
                            }
                        }
                    }
                }
                
                
                //check for XXO
                if (x < GridW - 2)
                {
                    if (GridSprite(x,y) == GridSprite(x + 1, y))
                    {
                        //check top
                        if (y < GridH - 2)
                        {
                            if (GridSprite(x,y) == GridSprite(x + 2, y + 1))
                            {
                                possibleMatches.Add(tileGrid[x, y]);
                                possibleMatches.Add(tileGrid[x + 1, y]);
                                possibleMatches.Add(tileGrid[x + 2, y + 1]);
                                return possibleMatches;
                            }
                        }
                        //check right
                        if (x < GridW - 3)
                        {
                            if (GridSprite(x,y) == GridSprite(x + 3, y))
                            {
                                possibleMatches.Add(tileGrid[x, y]);
                                possibleMatches.Add(tileGrid[x + 1, y]);
                                possibleMatches.Add(tileGrid[x + 3, y]);
                                return possibleMatches;
                            }
                        }
                        //check bottom
                        if (y > 0)
                        {
                            if (GridSprite(x,y) == GridSprite(x + 2, y - 1))
                            {
                                possibleMatches.Add(tileGrid[x, y]);
                                possibleMatches.Add(tileGrid[x + 1, y]);
                                possibleMatches.Add(tileGrid[x + 2, y - 1]);
                                return possibleMatches;
                            }
                        }
                    }
                }
                //check for OXX
                if (x < GridW - 2)
                {
                    if (GridSprite(x + 1, y) == GridSprite(x + 2, y))
                    {
                        //check top
                        if (y < GridH - 2)
                        {
                            if (GridSprite(x + 1, y) == GridSprite(x, y + 1))
                            {
                                possibleMatches.Add(tileGrid[x + 1, y]);
                                possibleMatches.Add(tileGrid[x + 2, y]);
                                possibleMatches.Add(tileGrid[x, y + 1]);
                                return possibleMatches;
                            }
                        }
                        //check left
                        if (x > 0)
                        {
                            if (GridSprite(x + 1, y) == GridSprite(x - 1, y)){
                                possibleMatches.Add(tileGrid[x + 1, y]);
                                possibleMatches.Add(tileGrid[x + 2, y]);
                                possibleMatches.Add(tileGrid[x - 1, y]);
                                return possibleMatches;
                            }
                        }
                        //check bottom
                        if (y > 0)
                        {
                            if (GridSprite(x + 1, y) == GridSprite(x, y - 1))
                            {
                                possibleMatches.Add(tileGrid[x + 1, y]);
                                possibleMatches.Add(tileGrid[x + 2, y]);
                                possibleMatches.Add(tileGrid[x, y - 1]);
                                return possibleMatches;
                            }
                        }
                    }
                }
                //check for XOX
                if (x < GridW - 2)
                {
                    if (GridSprite(x,y) == GridSprite(x + 2, y))
                    {
                        //check middle top
                        if (y < GridH - 1)
                        {
                            if (GridSprite(x,y) == GridSprite(x + 1, y + 1))
                            {
                                possibleMatches.Add(tileGrid[x, y]);
                                possibleMatches.Add(tileGrid[x + 2, y]);
                                possibleMatches.Add(tileGrid[x + 1, y + 1]);
                                return possibleMatches;
                            }
                        }
                        //check middle bottom
                        if (y > 0)
                        {
                            if (GridSprite(x,y) == GridSprite(x + 1, y - 1))
                            {
                                possibleMatches.Add(tileGrid[x, y]);
                                possibleMatches.Add(tileGrid[x + 2, y]);
                                possibleMatches.Add(tileGrid[x + 1, y - 1]);
                                return possibleMatches;
                            }
                        }
                    }
                }
            }
        }
        return possibleMatches;
    }

    public List<GameObject> GetTilesAroundOnlyDiagonal(GameObject obj)
    {
        List<GameObject> objects = new List<GameObject>();

        for (int x = 0; x < GridW; x++)
        {
            for (int y = 0; y < GridH; y++)
            {
                if (grid[x, y] == obj)
                {
                    //add top + right
                    if (y >= 0 && y < GridH - 1 && x >= 0 && x < GridW - 1)
                        objects.Add(grid[x + 1, y + 1]);
                    //add top + left
                    if (y >= 0 && y < GridH - 1 && x > 0)
                        objects.Add(grid[x - 1, y + 1]);
                    //add bottom + right
                    if (y > 0 && x >= 0 && x < GridW - 1)
                        objects.Add(grid[x + 1, y - 1]);
                    //add bottom + left
                    if (y > 0 && x > 0)
                        objects.Add(grid[x - 1, y - 1]);
                    return objects;
                }
            }
        }
        throw new Exception();
    }

    public List<GameObject> GetTilesAroundNotDiagonal(GameObject obj)
    {
        List<GameObject> objects = new List<GameObject>();

        for (int x = 0; x < GridW; x++)
        {
            for (int y = 0; y < GridH; y++)
            {
                if (grid[x, y] == obj)
                {
                    //add right
                    if (x >= 0 && x < GridW - 1)
                        objects.Add(grid[x + 1, y]);
                    //add left
                    if (x > 0)
                        objects.Add(grid[x - 1, y]);
                    // add top
                    if (y >= 0 && y < GridH - 1)
                        objects.Add(grid[x, y + 1]);
                    //add bottom
                    if (y > 0)
                        objects.Add(grid[x, y - 1]);
                    return objects;
                }
            }
        }
        throw new Exception();
    }

    public List<GameObject> GetTilesAround(GameObject obj)
    {
        List<GameObject> objects = new List<GameObject>();

        for (int x = 0; x < GridW; x++)
        {
            for (int y = 0; y < GridH; y++)
            {
                if (grid[x,y] == obj)
                {
                    //add right
                    if (x >= 0 && x < GridW - 1)
                        objects.Add(grid[x + 1, y]);
                    //add left
                    if (x > 0)
                        objects.Add(grid[x - 1, y]);
                    // add top
                    if (y >= 0 && y < GridH - 1)
                        objects.Add(grid[x, y + 1]);
                    //add bottom
                    if (y > 0)
                        objects.Add(grid[x, y - 1]);
                    //add top + right
                    if (y >= 0 && y < GridH - 1 && x >= 0 && x < GridW - 1)
                        objects.Add(grid[x + 1, y + 1]);
                    //add top + left
                    if (y >= 0 && y < GridH - 1 && x > 0)
                        objects.Add(grid[x - 1, y + 1]);
                    //add bottom + right
                    if (y > 0 && x >= 0 && x < GridW - 1)
                        objects.Add(grid[x + 1, y - 1]);
                    //add bottom + left
                    if (y > 0 && x > 0)
                        objects.Add(grid[x - 1, y - 1]);
                    return objects;
                }
            }
        }
        throw new Exception();
    }

    private IEnumerator ShiftTilesDown(int x, int yStart, float shiftDelay = .072f)
    {
        IsComputing = true;
        List<SpriteRenderer> renderers = new List<SpriteRenderer>();
        int nullCount = 0;

        //find all null sprites
        for (int y = yStart; y < GridH; y++)
        {
            SpriteRenderer renderer = tileGrid[x, y].Renderer;
            //SpriteRenderer renderer = grid[x, y].GetComponent<SpriteRenderer>();
            if (renderer.sprite == null)
            {
                nullCount++;
            }
            renderers.Add(renderer);
        }

        //let all null sprites fall
        for (int i = 0; i < nullCount; i++)
        {
            yield return new WaitForSeconds(shiftDelay);
            for (int k = 0; k < renderers.Count - 1; k++)
            {
                renderers[k].sprite = renderers[k + 1].sprite;
                renderers[k + 1].sprite = GenerateNewSprite(x, GridH - 1);
            }
        }
        //check first line for null sprites
        for (int x1 = 0; x1 < GridW; x1++)
        {
            if (tileGrid[x1, GridH - 1].Renderer.sprite == null)
                tileGrid[x1, GridH - 1].Renderer.sprite = GenerateNewSprite(x1, GridH - 1);
            //if (grid[x1, GridH - 1].GetComponent<SpriteRenderer>().sprite == null)
            //    grid[x1, GridH - 1].GetComponent<SpriteRenderer>().sprite = GenerateNewSprite(x1, GridH - 1);
        }
        //if there's any hint available, disable it because a match happened       
        UnHighlightTiles();
        IsComputing = false;

        ResetHintTimer();
        highlightedTiles = GetFirstMatchAvailable();
        CheckGameOver();
    }

    private void CheckGameOver()
    {
        if (highlightedTiles.Count < 3)
            SetGameOver();
    }

    private Sprite GenerateNewSprite(int x, int y)
    {
        List<Sprite> possibleSpawnable = new List<Sprite>();
        possibleSpawnable.AddRange(settings.SpawnableObjects);

        //remove left
        if (x > 0)
            possibleSpawnable.Remove(GridSprite(x - 1, y));
        //possibleSpawnable.Remove(grid[x - 1, y].GetComponent<SpriteRenderer>().sprite);
        //remove right
        if (x < GridW - 1)
            possibleSpawnable.Remove(GridSprite(x + 1, y));
        //possibleSpawnable.Remove(grid[x + 1, y].GetComponent<SpriteRenderer>().sprite);        
        //remove bottom
        if (y > 0)
            possibleSpawnable.Remove(GridSprite(x, y - 1));
            //possibleSpawnable.Remove(grid[x, y - 1].GetComponent<SpriteRenderer>().sprite);

        if (UnityEngine.Random.Range(0, 100) < settings.PowerupSpawnChance)
            return settings.PowerUp;
        else
            return possibleSpawnable[UnityEngine.Random.Range(0, possibleSpawnable.Count)];
    }

    private void UnHighlightTiles()
    {
        if (highlightedTiles.Count > 0)
            foreach (Tile tile in highlightedTiles)
            {
                tile.DisableHighlight();
            }
        highlightedTiles = new List<Tile>();
    }

    private void HighlightTiles()
    {
        foreach (Tile tile in highlightedTiles)
            tile.Highlight();
    }

    private void HintTiles()
    {
        if (hintTimer <= 0)
        {
            highlightedTiles = GetFirstMatchAvailable();
            HighlightTiles();
            ResetHintTimer();
        }
        else
            hintTimer -= Time.deltaTime;
    }

    // Update is called once per frame
    void Update()
    {
        switch (GameState)
        {
            case GameState.Normal:
                HintTiles();
                break;
            case GameState.OnPause:

                break;
            case GameState.GameOver:
                GameOver();
                break;
            default:
                throw new ArgumentException("there's no default state in gamestate");
        }
        UpdateScore();
    }

    private void ResetHintTimer()
    {
        hintTimer = settings.SecondsBeforeHint;
    }

    private void GameOver()
    {
        gameObject.SetActive(false);
        GameObject.Find("ConfirmButton").GetComponent<SendToHighscore>().SummonForm();
    }

    public void SetGameOver()
    {
        GameState = GameState.GameOver;
    }

    private void UpdateScore()
    {
        scoreText.text = "Score: " + settings.Score;
    }
}
