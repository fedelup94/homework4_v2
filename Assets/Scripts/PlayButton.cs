﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayButton : MonoBehaviour
{
    private GameObject bg;
    private Color color;

    private void Start()
    {
        bg = GameObject.Find("FadeBg");
        color = bg.GetComponent<SpriteRenderer>().color;
    }

    private void OnMouseDown()
    {
        
        bg.transform.localPosition = new Vector3(0, 0, -1000);
        StartCoroutine(FadeOut(2f));
        AudioManager.Instance.PlaySound(AudioClip.Clear);
    }

    private IEnumerator FadeOut(float delay)
    {
        float elapsedTime = 0f;
        color = Color.black;
        color.a = 0f;
        while (color.a < 1f)
        {
            elapsedTime += Time.deltaTime;
            color.a += Mathf.Clamp(elapsedTime, 0, delay);
            //color = Color.Lerp(color, new Color(color.r,color.g,color.b,color.a++), elapsedTime);
            yield return null;
        }
        SceneManager.LoadSceneAsync(1, LoadSceneMode.Single);

    }
}
