﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable]
public class HighScore : IComparable
{
    private int score;
    private string user;

    public HighScore(string user, int score)
    {
        this.score = score;
        this.user = user;
    }


    public string User { get => user; set => user = value; }
    public int Score { get => score; set => score = value; }

    public int CompareTo(object obj)
    {
        if (obj == null)
            return 1;
        HighScore other = obj as HighScore;
        if (other != null)
            return other.Score.CompareTo(Score);
        else
            throw new ArgumentException("Not an highscore");
    }
}

[Serializable]
public class HighscoreData
{
    private List<HighScore> scores;


    public HighscoreData()
    {
        Scores = new List<HighScore>();
        
    }

    public HighscoreData(string newEntry, int newScore)
    {
        Scores = new List<HighScore>
        {
            new HighScore(newEntry, newScore)
        };
    }

    public HighscoreData(HighScore highScore)
    {
        scores = new List<HighScore>
        {
            highScore
        };
    }

    public List<HighScore> Scores { get => scores; set => scores = value; }


}

