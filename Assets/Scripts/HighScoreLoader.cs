﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable]
public class HighScoreLoader : MonoBehaviour
{

    private HighscoreData data;

    // Start is called before the first frame update
    void Start()
    {
        HighScore temp = Settings.Instance.PopScore();
        if (temp != null)
            data = new HighscoreData(temp);
        else
            data = new HighscoreData();
        Destroy(Settings.Instance.gameObject);

        LoadFile();
        SaveFile();
        SortScores();
        ShowScores();
    }

    private void SortScores()
    {
        data.Scores.Sort();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void ShowScores()
    {
        
        if (data.Scores.Count > 0)
        {
            List<Transform> transforms = GetChildren();
            for (int i = 0; i < transforms.Count && i < data.Scores.Count; i++)
            {

                TMPro.TextMeshProUGUI user = transforms[i].Find("NameText").GetComponent<TMPro.TextMeshProUGUI>();
                TMPro.TextMeshProUGUI score = transforms[i].Find("ScoreText").GetComponent<TMPro.TextMeshProUGUI>();

                user.text = data.Scores[i].User;
                score.text = data.Scores[i].Score.ToString();


            }
        }
    }

    private List<Transform> GetChildren()
    {
        List<Transform> arrayOfHighscores = new List<Transform>();

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).name.Contains("highscore"))
                arrayOfHighscores.Add(transform.GetChild(i));
        }
        return arrayOfHighscores;
    }

    private void LoadFile()
    {
        string destinationFile = Application.persistentDataPath + "/hs.dat";
        FileStream file;

        if (File.Exists(destinationFile))
        {
            file = File.OpenRead(destinationFile);
            BinaryFormatter bf = new BinaryFormatter();
            HighscoreData new_data = (HighscoreData)bf.Deserialize(file);
            file.Close();
            data.Scores.AddRange(new_data.Scores);
        }
        

        

    }

    private void SaveFile()
    {
        string destinationFile = Application.persistentDataPath + "/hs.dat";
        FileStream file;

        if (File.Exists(destinationFile))
            file = File.OpenWrite(destinationFile);
        else
            file = File.Create(destinationFile);

        BinaryFormatter bf = new BinaryFormatter();
        bf.Serialize(file, data);
        file.Close();
    }
}
