﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerPick : MonoBehaviour
{
    [SerializeField]
    private Transform ice;
    [SerializeField]
    private Transform bomb;
    [SerializeField]
    private GameObject particlePrefab;


    private TMPro.TextMeshProUGUI iceText;
    private TMPro.TextMeshProUGUI bombText;

    private GameObject particle = null;
    private Sprite chosenPowerup;
    private bool isBombPowerup;

    public bool IsBombPowerup { get => isBombPowerup; private set => isBombPowerup = value; }
    public Sprite ChosenPowerup { get => chosenPowerup; private set => chosenPowerup = value; }

    // Start is called before the first frame update

    private void Awake()
    {

    }

    void Start()
    {
        ChosenPowerup = bomb.GetComponent<SpriteRenderer>().sprite;
        IsBombPowerup = true;
        bombText = bomb.GetComponentInChildren<TMPro.TextMeshProUGUI>();
        iceText = ice.GetComponentInChildren<TMPro.TextMeshProUGUI>();
        bombText.fontStyle = TMPro.FontStyles.Strikethrough;


    }

    public void SetBombPowerup()
    {
        if (particle == null)
        {
            particle = Instantiate(particlePrefab, bomb.Find("ParticleSpawnPoint"));
        }
        particle.transform.SetParent(bomb.Find("ParticleSpawnPoint"));
        particle.transform.localPosition = new Vector3(0,.5f,0);
        particle.GetComponent<ParticleSystem>().Play();
        particle.AddComponent<ParticleAutoKill>();
        ChosenPowerup = bomb.GetComponent<SpriteRenderer>().sprite;
        IsBombPowerup = true;
        bombText.fontStyle = TMPro.FontStyles.Strikethrough;
        iceText.fontStyle = TMPro.FontStyles.Normal;
        AudioManager.Instance.PlaySound(AudioClip.Bomb);
    }

    public void SetIcePowerup()
    {
        if (particle == null)
        {
            particle = Instantiate(particlePrefab, ice.Find("ParticleSpawnPoint"));
        }
        particle.transform.SetParent(ice.Find("ParticleSpawnPoint"));
        particle.transform.localPosition = new Vector3(0,.5f,0);
        particle.GetComponent<ParticleSystem>().Play();
        particle.AddComponent<ParticleAutoKill>();
        ChosenPowerup = ice.GetComponent<SpriteRenderer>().sprite;
        IsBombPowerup = false;
        iceText.fontStyle = TMPro.FontStyles.Strikethrough;
        bombText.fontStyle = TMPro.FontStyles.Normal;
        AudioManager.Instance.PlaySound(AudioClip.Freeze);
    }

    private void OnDestroy()
    {
        Settings.Instance.SetPowerUp(ChosenPowerup);
    }
}
