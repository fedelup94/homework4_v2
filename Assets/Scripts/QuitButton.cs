﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuitButton : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        if (BoardManager.Instance.GameState == GameState.OnPause)
        {
            Settings.Instance.ResetSettings();
            SceneManager.LoadScene(0);
        }
        else
            throw new Exception("Can't click on this button if it's not on pause");
        
    }
}
