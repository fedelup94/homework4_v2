﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{

    public enum OtherTileDirection { LEFT, RIGHT, UP, DOWN };

    private static Color selectedColor = new Color(.5f, .5f, .5f, 1f);
    private static Color hintColor = new Color(1f, 1f, 1f, .1f);
    private static Tile previousSelectedTile;


    private Animator animator;
    private OtherTileDirection otherTileDirection;
    private new SpriteRenderer renderer;
    private bool isSelected = false;
    private bool matchFound = false;
    private bool isHighlighted;

    private readonly Vector2[] adjacentDirections = new Vector2[] { Vector2.up, Vector2.right, Vector2.down, Vector2.left };

    public SpriteRenderer Renderer { get => renderer; private set => renderer = value; }

    public static OtherTileDirection GetOpposite(OtherTileDirection direction)
    {
        switch (direction)
        {
            case OtherTileDirection.LEFT:
                return OtherTileDirection.RIGHT;
            case OtherTileDirection.RIGHT:
                return OtherTileDirection.LEFT;
            case OtherTileDirection.UP:
                return OtherTileDirection.DOWN;
            case OtherTileDirection.DOWN:
                return OtherTileDirection.UP;
            default:
                throw new ArgumentNullException("Can't find the Direction");
        }
    }

    public void Highlight()
    {
        StartCoroutine(Flash());
        //renderer.color = hintColor;
    }

    private IEnumerator Flash()
    {
        isHighlighted = true;
        while (isHighlighted)
        {
            Renderer.color = (Renderer.color == hintColor) ? Color.white : hintColor;
            yield return new WaitForSeconds(0.6f) ;
        }
        Renderer.color = Color.white;
    }

    public void DisableHighlight()
    {
        isHighlighted = false;
        Renderer.color = Color.white;
    }

    void Awake()
    {
        Renderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }

    void OnMouseDown()
    {

        if (Renderer.sprite == null || BoardManager.Instance.IsComputing || BoardManager.Instance.GameState != GameState.Normal)
        {
            return;
        }

        if (isSelected)
        {
            Deselect();
        }
        else
        {
            if (previousSelectedTile == null)
                Select();
            else
            {
                if (GetAllAdjacent().Contains(previousSelectedTile.gameObject) )
                {
                    otherTileDirection = CalculateDirection(previousSelectedTile);
                    SwapTile(previousSelectedTile);
                    int mostMatch = Math.Max(GetMatchesHorizontal().Count, GetMatchesVertical().Count);
                    int previousTileMostMatch = Math.Max(previousSelectedTile.GetMatchesHorizontal().Count, previousSelectedTile.GetMatchesVertical().Count);
                    //check if there are enough tiles of the same type
                    if (mostMatch < Settings.Instance.TileToGetMatch - 1 &&previousTileMostMatch < Settings.Instance.TileToGetMatch - 1)
                    {
                        SwapTile(previousSelectedTile);
                        previousSelectedTile.Deselect();
                        Select();
                    }
                    else
                    {
                        AudioManager.Instance.PlaySound(AudioClip.Swap);
                        previousSelectedTile.ComputeMatches();
                        previousSelectedTile.Deselect();
                        ComputeMatches();
                    }
                }
                else
                {
                    previousSelectedTile.Deselect();
                    Select();
                }
            }
        }
    }

    private OtherTileDirection CalculateDirection(Tile previousSelectedTile)
    {

        if (GetAdjacent(adjacentDirections[0]) == previousSelectedTile.gameObject)
            return OtherTileDirection.UP;
        if (GetAdjacent(adjacentDirections[1]) == previousSelectedTile.gameObject)
            return OtherTileDirection.RIGHT;
        if (GetAdjacent(adjacentDirections[2]) == previousSelectedTile.gameObject)
            return OtherTileDirection.DOWN;
        if (GetAdjacent(adjacentDirections[3]) == previousSelectedTile.gameObject)
            return OtherTileDirection.LEFT;
        throw new ArgumentNullException("Can't find the right direction");
    }

    public void SwapTile(Tile otherRenderer)
    {
        if (Renderer.sprite == otherRenderer.Renderer.sprite)
            return;

        StartCoroutine(SwapSpriteAnimation(otherRenderer));

        Debug.Log(otherTileDirection.ToString());
        //sound
    }

    private IEnumerator SwapSpriteAnimation(Tile other, float delay = 0.72f)
    {
        Sprite tempSprite = other.Renderer.sprite;
        other.Renderer.sprite = Renderer.sprite;
        Renderer.sprite = tempSprite;

        yield return new WaitForSeconds(delay);

    }

    private IEnumerator SwapAnimation(Tile other, Vector3 o1Pos, Vector3 o2Pos)
    {
        float timeSinceStarted = 0f;
        //Vector3 o1Pos = gameObject.transform.position;
        //Vector3 o2Pos = other.gameObject.transform.position;

        while (true)
        {
            timeSinceStarted += Time.deltaTime;
            transform.position = Vector3.Lerp(transform.position, o2Pos, timeSinceStarted);
            other.transform.position = Vector3.Lerp(other.transform.position, o1Pos, timeSinceStarted);
            if (transform.position == o2Pos && other.transform.position == o1Pos)
                yield break;
            else
            {
                transform.position = o1Pos;
                other.transform.position = o2Pos;
                yield return null;
            }
        }
    }

    private List<GameObject> FindMatch(Vector2 castDir)
    {
        List<GameObject> matches = new List<GameObject>();
        RaycastHit2D hit = Physics2D.Raycast(transform.position, castDir);
        while (hit.collider != null && hit.collider.GetComponent<SpriteRenderer>().sprite == Renderer.sprite)
        {
            matches.Add(hit.collider.gameObject);
            hit = Physics2D.Raycast(hit.collider.transform.position, castDir);
        }
        return matches;
    }

    public void ComputeMatches()
    {
        if (Renderer.sprite == null)
            return;
        ComputeMatch(new Vector2[] { Vector2.left, Vector2.right });
        ComputeMatch(new Vector2[] { Vector2.up, Vector2.down });

        if (matchFound)
        {
            SpawnDyingTile(Renderer.gameObject);
            Renderer.sprite = null;
            matchFound = false;
            StopCoroutine(BoardManager.Instance.FindNulltiles());
            StartCoroutine(BoardManager.Instance.FindNulltiles());
            AudioManager.Instance.PlaySound(AudioClip.Clear);
        }

    }



    private GameObject SpawnDyingTile(GameObject gameObject)
    {
        if (gameObject.GetComponent<SpriteRenderer>().sprite != Settings.Instance.PowerUp)
        {
            GameObject temp = Instantiate(gameObject, gameObject.transform);
            temp.GetComponent<SpriteRenderer>().color = Color.white;
            temp.transform.localScale = new Vector3(1, 1, 1);
            temp.transform.localPosition = new Vector3();
            temp.AddComponent<DyingTile>();
            return temp;
        }
        else
            return null;

    }

    private void ComputeMatch(Vector2[] paths)
    {
        List<GameObject> matchingTiles = new List<GameObject>();

        for (int i = 0; i < paths.Length; i++)
        {
            matchingTiles.AddRange(FindMatch(paths[i]));
        }

        if (matchingTiles.Count >= Settings.Instance.TileToGetMatch - 1)
        {
            if (Settings.Instance.IsPowerUpBomb)
            {
                List<GameObject> tempL = new List<GameObject>();
                for (int i = 0; i < matchingTiles.Count; i++)
                {
                    foreach (GameObject obj in BoardManager.Instance.GetTilesAroundNotDiagonal(matchingTiles[i]))
                    {
                        if (obj.GetComponent<SpriteRenderer>().sprite == Settings.Instance.PowerUp)
                        {
                            tempL.Add(obj);
                            //AddParticle(obj);
                            Settings.Instance.AddScore(Settings.Instance.PowerupScore);
                            tempL.AddRange(BoardManager.Instance.GetTilesAround(obj));
                            AudioManager.Instance.PlaySound(AudioClip.Bomb);
                        }                         
                    }
                }
                //check this tile too
                foreach (GameObject obj in BoardManager.Instance.GetTilesAroundNotDiagonal(gameObject))
                {
                    if (obj.GetComponent<SpriteRenderer>().sprite == Settings.Instance.PowerUp)
                    {
                        tempL.Add(obj);
                        Settings.Instance.AddScore(Settings.Instance.PowerupScore);
                        tempL.AddRange(BoardManager.Instance.GetTilesAround(obj));
                        AudioManager.Instance.PlaySound(AudioClip.Bomb);
                    }
                }
                matchingTiles.AddRange(tempL);
            }
            else
            {
                List<GameObject> tempL = new List<GameObject>();
                for (int i = 0; i < matchingTiles.Count; i++)
                {
                    foreach (GameObject obj in BoardManager.Instance.GetTilesAroundNotDiagonal(matchingTiles[i]))
                    {
                        if (obj.GetComponent<SpriteRenderer>().sprite == Settings.Instance.PowerUp)
                        {
                            Timer.instance.AddFreezeTime(Settings.Instance.PowerupTime);
                            tempL.Add(obj);
                            AudioManager.Instance.PlaySound(AudioClip.Freeze);
                        }
                    }
                }
                //check this tile too
                foreach (GameObject obj in BoardManager.Instance.GetTilesAroundNotDiagonal(gameObject))
                {
                    if (obj.GetComponent<SpriteRenderer>().sprite == Settings.Instance.PowerUp)
                    {
                        tempL.Add(obj);
                        //AddParticle(obj);
                        Timer.instance.AddFreezeTime(Settings.Instance.PowerupTime);
                        AudioManager.Instance.PlaySound(AudioClip.Freeze);
                    }
                }
                matchingTiles.AddRange(tempL);
            }

            //make the matching tiles null
            for (int i = 0; i < matchingTiles.Count; i++)
            {
                AddParticle(matchingTiles[i]);
                SpawnDyingTile(matchingTiles[i]);
                matchingTiles[i].GetComponent<SpriteRenderer>().sprite = null;
            }
            int multiplier = matchingTiles.Count / Settings.Instance.TileToGetMatch;
            multiplier = (multiplier<=1) ? 1 : multiplier;
            Settings.Instance.AddScore(Settings.Instance.FruitScore * matchingTiles.Count, multiplier);
            matchFound = true;
        }
    }

    private void AddParticle(GameObject obj)
    {
        GameObject particle = Instantiate(Settings.Instance.ParticlePrefab, obj.transform);
        particle.transform.localPosition = new Vector3();
        particle.GetComponent<ParticleSystem>().Play();
        particle.AddComponent<ParticleAutoKill>();
    }

    public List<GameObject> GetMatchesHorizontal()
    {
        List<GameObject> matches = new List<GameObject>();
        matches.AddRange(GetMatchesPerDirection(new Vector2[] { Vector2.left, Vector2.right }));
        return matches;
    }

    public List<GameObject> GetMatchesVertical()
    {
        List<GameObject> matches = new List<GameObject>();
        matches.AddRange(GetMatchesPerDirection(new Vector2[] { Vector2.up, Vector2.down }));
        return matches;
    }

    private List<GameObject> GetMatchesPerDirection(Vector2[] paths)
    {
        List<GameObject> matches = new List<GameObject>();

        for (int i = 0; i < paths.Length; i++)
        {
            matches.AddRange(FindMatch(paths[i]));
        }
        return matches;

    }

    private List<GameObject> GetAllAdjacent()
    {
        List<GameObject> adjacentTiles = new List<GameObject>();

        for (int i = 0; i < adjacentDirections.Length; i++)
        {
            adjacentTiles.Add(GetAdjacent(adjacentDirections[i]));
        }
        return adjacentTiles;
    }

    private GameObject GetAdjacent(Vector2 castDir)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, castDir);
        if (hit.collider != null)            
            return hit.collider.gameObject;
        return null;
    }

    private void Select()
    {
        isSelected = true;
        Renderer.color = selectedColor;
        previousSelectedTile = gameObject.GetComponent<Tile>();
        //play sound
    }

    private void Deselect()
    {
        isSelected = false;
        Renderer.color = Color.white;
        previousSelectedTile = null;
    }


}
