﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour
{

    private static Settings instance;

    [Header("Powerup And Sprites")]
    [Tooltip("Put all spawnable fruit sprites in here")]
    [SerializeField]
    private List<Sprite> spawnableObjects = new List<Sprite>();
    [Tooltip("Put on position 0 put the bomb, on position 1 the freeze")]
    [SerializeField]
    private List<Sprite> possiblePowerups = new List<Sprite>();


    [Header("Settings")]
    [SerializeField]
    private GameObject tilePrefab;
    [SerializeField]
    private int gridW, gridH;
    [Tooltip("Powerup Spawn Chance (0-100)")]
    [SerializeField]
    private int powerupChance = 2;
    [Tooltip("Only 3 tiles supported for match prediction")]
    [SerializeField]
    private int tileToGetMatch = 3;
    


    [Header("Timer")]
    [Tooltip("Timer in seconds to solve the puzzle")]
    [SerializeField]
    private float secondsToSolve = 300;
    [Tooltip("Timer in seconds before the game highlights the hint")]
    [SerializeField]
    private float secondsBeforeHint = 15f;

    [Header("Score and Powerup")]
    [Tooltip("Score gained for each fruit")]
    [SerializeField]
    private int fruitScore = 10;
    [Tooltip("Value of the bomb powerup")]
    [SerializeField]
    private int powerupScore = 200;
    [Tooltip("Value of the ice powerup")]
    [SerializeField]
    private float powerupTime = 5f;


    [Header("Particles")]
    [SerializeField]
    private GameObject particlePrefab;

    private bool isPowerUpBomb;
    private int score;
    private Sprite powerUp;
    private HighScore highscore;


    public int TileToGetMatch { get => tileToGetMatch; private set => tileToGetMatch = value; }
    public Sprite PowerUp { get => powerUp; private set => powerUp = value; }
    public bool IsPowerUpBomb { get => isPowerUpBomb; private set => isPowerUpBomb = value; }
    public int FruitScore { get => fruitScore; private set => fruitScore = value; }
    public int PowerupScore { get => powerupScore; private set => powerupScore = value; }
    public float PowerupTime { get => powerupTime; private set => powerupTime = value; }
    public int Score { get => score; private set => score = value; }
    public GameObject ParticlePrefab { get => particlePrefab; private set => particlePrefab = value; }
    public List<Sprite> SpawnableObjects { get => spawnableObjects; set => spawnableObjects = value; }
    public int PowerupSpawnChance { get => powerupChance; set => powerupChance = value; }
    public float SecondsToSolve { get => secondsToSolve; private set => secondsToSolve = value; }
    public GameObject TilePrefab { get => tilePrefab; set => tilePrefab = value; }
    public int GridW { get => gridW; set => gridW = value; }
    public int GridH { get => gridH; set => gridH = value; }
    public float SecondsBeforeHint { get => secondsBeforeHint; set => secondsBeforeHint = value; }
    public static Settings Instance { get => instance; }

    void Awake()
    {
        if (instance != null && instance != this)
            Destroy(this.gameObject);
        else
            instance = this;
    }

    public void SetHighScore(HighScore score)
    {
        highscore = score;
    }

    public HighScore PopScore()
    {
        if (highscore != null)
        {
            HighScore temp = highscore;
            highscore = null;
            return temp;
        }
        return null;
    }

    public void SetPowerUp(Sprite sprite)
    {
        if (powerUp == null)
        {
            //if it's a bomb (0 in list)
            isPowerUpBomb = (sprite == possiblePowerups[0]) ? true : false;
            powerUp = sprite;
        }
    }

    public void ResetSettings()
    {
        score = 0;
        powerUp = null;
    }

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void AddScore(int value)
    {
        Score += value;
    }

    public void AddScore(int value, int multiplier)
    {
        Score += value * multiplier;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
