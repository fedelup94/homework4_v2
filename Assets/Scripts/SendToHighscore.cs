﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SendToHighscore : MonoBehaviour
{

    [SerializeField]
    private RectTransform saveNewRecordCanvas;

    private void OnMouseDown()
    {
        string user;
        user = transform.parent.parent.GetComponentInChildren<TMPro.TMP_InputField>().text;
        user = (user.Length > 5) ? user.Substring(0, 5) : user;

        Settings.Instance.SetHighScore(new HighScore(user, Settings.Instance.Score));
        SceneManager.LoadScene(2);

    }

    public void SummonForm()
    {
        if (BoardManager.Instance.GameState == GameState.GameOver)
        {
            StartCoroutine(MoveTo(new Vector2()));
        }
    }

    private IEnumerator MoveTo(Vector2 pos, float speedMulti = 1)
    {
        float timeSinceStarted = 0f;

        while (true)
        {
            timeSinceStarted += Time.deltaTime * speedMulti;
            saveNewRecordCanvas.anchoredPosition = Vector2.Lerp(saveNewRecordCanvas.anchoredPosition, pos, timeSinceStarted);
            if (saveNewRecordCanvas.anchoredPosition == pos)
            {
                yield break;
            }
            else
                yield return null;
        }
    }

}
