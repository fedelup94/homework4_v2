﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    [SerializeField]
    private RectTransform menuCanvas;
    [SerializeField]
    private Sprite pauseOnSprite;
    [SerializeField]
    private Sprite pauseOffSprite;

    private SpriteRenderer renderer;
    private Vector2 oldTransform;

    private void Awake()
    {
        renderer = GetComponent<SpriteRenderer>();
    }

    void OnMouseDown()
    {
        if (!BoardManager.Instance.IsComputing && BoardManager.Instance.GameState != GameState.GameOver)
        {
            if (BoardManager.Instance.GameState == GameState.Normal)
            {
                StartCoroutine(MoveTo(new Vector2()));
                BoardManager.Instance.SetOnPause();
                renderer.sprite = pauseOnSprite;
            }
            else
            {
                StopAllCoroutines();
                StartCoroutine(MoveTo(oldTransform,3));
                BoardManager.Instance.UnPause();
                renderer.sprite = pauseOffSprite;
            }
        }

    }

    private IEnumerator MoveTo(Vector2 pos, float speedMulti = 1)
    {
        float timeSinceStarted = 0f;

        while (true)
        {
            timeSinceStarted += Time.deltaTime * speedMulti;
            menuCanvas.anchoredPosition = Vector2.Lerp(menuCanvas.anchoredPosition, pos, timeSinceStarted);
            if (menuCanvas.anchoredPosition == pos)
            {
                yield break;
            }
            else
                yield return null;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        oldTransform = menuCanvas.anchoredPosition;
    }

    // Update is called once per frame
    void Update()
    {
        if (BoardManager.Instance.GameState == GameState.GameOver)
            gameObject.SetActive(false);
    }
}
